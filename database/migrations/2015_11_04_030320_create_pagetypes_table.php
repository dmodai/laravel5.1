<?php
//php artisan make:model Article
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagetypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		/*Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
		});*/
        Schema::create('pagetypes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('type')->nullable();
            $table->string('tag')->nullable();
            $table->integer('user_id');
            $table->integer('pid');
            $table->timestamps();
            $table->string('slug')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
