<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');*/
/*Route::get('/', 'HomeController@index');
Route::get('pages/{id}', 'PagesController@show');*/

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

/*
Route::get('/', function(){
   return View::make('index');
});*/

Route::group(array('prefix' => 'api', 'namespace' => 'Api'), function() {
    Route:get('/', 'PagesController@Index');
    Route::resource('comments', 'CommentsController',
        array('only' => array('index', 'store', 'destroy')));
    Route::resource('pages', 'Pagescontroller',
        array('only' => array('index', 'store', 'destroy')));
});

/*
Route::get('/', 'HomeController@index');

Route::get('pages/{id}', 'PagesController@show');
Route::post('comment/store', 'CommentsController@store');*/

/*
 但这种方法非常不推荐大家使用。上面两个分别是用于 认证(auth) 和 重置密码(password) 的。

两个文件分别为 app\Http\Controllers\Auth\AuthController.php 和 app\Http\Controllers\Auth\PasswordController.php。详细代码自己查看文件内容，实现代码其实在文件 vendor\laravel\framework\src\Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers.php 中。*/
/*Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);*/
//只保留基本的登录、注销（注册）功能。
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::post('auth/logout', 'Auth\AuthController@postLogout');
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
Route::controllers([
    'password' => 'Auth\PasswordController',
]);


//Route::get('mail/send','MailController@send');

Route::resource('user', 'UserController');




Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function(){
    Route:get('/', 'AdminHomeController@Index');
    Route::resource('pages', 'PagesController');
    Route::resource('comments', 'CommentsController');
});

//sql 预处理 语句var_dump($bindings);// 替换数据
//Event::listen("illuminate.query", function($query, $bindings){var_dump($query);var_dump($bindings);echo "</br>";});


