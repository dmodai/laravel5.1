<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
//use Symfony\Component\HttpFoundation\Request as RequestHttp;
use Response;

use App\Page;

use Redirect, Input, Auth;

class PagesController extends Controller {

    public function index(Request $request){

        if ($request->ajax()){
            //return jsonp
          /*  $callback = input::get('callback');
            return $callback."(".json_encode(Page::get()).")";*/
            return Response::json(Page::get());
        }else{
            return Response::json(Page::get());
        }

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:pages|max:255',
            'body' => 'required',
        ]);

        $page = new Page;
        $page->title = Input::get('title');
        $page->body = Input::get('body');
        $page->user_id = 1;//Auth::user()->id;

        if ($request->ajax()) {
            if($page->save()){
                return Response::json(array('success' => true, 'type'=>'create'));
            }else{
                return Response::json(array('success' => false, 'type'=>'create'));
            }
        }else{
            if ($page->save()) {
                return Redirect::to('admin');
            } else {
                return Redirect::back()->withInput()->withErrors('保存失败！');
            }
        }

     /*   Page::create(array(
            'title' => Input::get('title'),
            'body' => Input::get('body')
        ));
        return Resoponse::json(array('success' => true));*/

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin.pages.edit')->withPage(Page::find($id));
    }

    public function show(Request $request, $id){
        if ($request -> ajax()) {

        }else{
            return view('admin.pages.show')->withPage(Page::find($id));
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|unique:pages,title,'.$id.'|max:255',
            'body' => 'required',
        ]);

        $page = Page::find($id);
        $page->title = Input::get('title');
        $page->body = Input::get('body');
        $page->user_id = 1;//Auth::user()->id;
        if ($request->ajax()) {

        }else{
            if ($page->save()) {
                return Redirect::to('admin');
            } else {
                return Redirect::back()->withInput()->withErrors('保存失败！');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->ajax()) {
            if(Page::destroy($id)){
                return Response::json(array('success' => true, 'type' => 'delete'));
            }else{
                return Response::json(array('success' => false, 'type' => 'delete'));
            }
        }else{
            $page = Page::find($id);
            $page->delete();
            return Redirect::to('admin');
        }

    }

}