<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Response;

use App\Page;
use Redirect, Input, Auth;

class PagesController extends Controller {

    public function index(){
        //return jsonp
       /* $callback = input::get('callback');
        return $callback."(".json_encode(Page::get()).")";*/
        return Response::json(Page::get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:pages|max:255',
            'body' => 'required',
        ]);

        $page = new Page;
        $page->title = Input::get('title');
        $page->body = Input::get('body');
        $page->user_id = 1;//Auth::user()->id;
        /* Page::create(array(
             'title' => Input::get('title'),
             'body' => Input::get('body'),
             'user_id' => 1,
         ));*/
        if($page->save()){
            return Response::json(array('success' => true, 'type'=>'create'));
        }else{
            return Response::json(array('success' => false, 'type'=>'create'));
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */


    public function show($id){
        return view('admin.pages.show')->withPage(Page::find($id));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|unique:pages,title,'.$id.'|max:255',
            'body' => 'required',
        ]);

        $page = Page::find($id);
        $page->title = Input::get('title');
        $page->body = Input::get('body');
        $page->user_id = 1;//Auth::user()->id;

        if ($page->save()) {
            return Redirect::to('admin');
        } else {
            return Redirect::back()->withInput()->withErrors('保存失败！');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
       /* $page = Page::find($id);
        $page->delete();

        return Redirect::to('admin');*/
        if(Page::destroy($id)){
            return Response::json(array('success' => true, 'type' => 'delete'));
        }else{
            return Response::json(array('success' => false, 'type' => 'delete'));
        }
    }

}